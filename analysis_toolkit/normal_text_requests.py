import requests
import html2text


def to_terminal(url: str):
    r = requests.get(url)
    lines = html2text.html2text(r.text).split("\n")
    for line in lines:
        print(line)


def to_textfile(url: str):
    r = requests.get(url)
    txt = html2text.html2text(r.text)
    with open("./test.txt", 'w') as f:
        f.write(txt)


def return_it(url: str) -> str:
    r = requests.get(url)
    return html2text.html2text(r.text)

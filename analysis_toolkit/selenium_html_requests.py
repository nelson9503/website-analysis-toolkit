from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import chromedriver_autoinstaller as driverauto

def to_textfile(url: str):
    path = driverauto.install()
    chrome = webdriver.Chrome(path)
    chrome.get(url)
    txt = chrome.page_source
    chrome.quit()
    lines = txt.split("<")
    txt = lines[0]
    with open("test.txt", 'w') as f:
        for i in range(1, len(lines)):
            try:
                f.write("<"+lines[i]+"\n")
            except:
                pass

def return_it(url: str):
    path = driverauto.install()
    chrome = webdriver.Chrome(path)
    chrome.get(url)
    txt = chrome.page_source
    chrome.quit()
    return txt
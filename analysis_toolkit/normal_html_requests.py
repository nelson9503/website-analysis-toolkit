import requests


def to_textfile(url: str):
    r = requests.get(url)
    txt = r.text
    lines = txt.split("<")
    txt = lines[0]
    with open("test.txt", 'w') as f:
        for i in range(1, len(lines)):
            try:
                f.write("<"+lines[i]+"\n")
            except:
                pass


def return_it(url: str) -> str:
    r = requests.get(url)
    return r.text

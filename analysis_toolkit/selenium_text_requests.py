import html2text
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import chromedriver_autoinstaller as driverauto


def to_terminal(url: str):
    path = driverauto.install()
    chrome = webdriver.Chrome(path)
    chrome.get(url)
    html = chrome.page_source
    lines = html2text.html2text(html).split("\n")
    chrome.quit()
    for line in lines:
        print(line)

def to_textfile(url: str):
    path = driverauto.install()
    chrome = webdriver.Chrome(path)
    chrome.get(url)
    html = chrome.page_source
    lines = html2text.html2text(html)
    chrome.quit()
    with open("test.txt", 'w') as f:
        f.write(lines)

def return_it(url: str):
    path = driverauto.install()
    chrome = webdriver.Chrome(path)
    chrome.get(url)
    html = chrome.page_source
    lines = html2text.html2text(html)
    chrome.quit()
    return lines